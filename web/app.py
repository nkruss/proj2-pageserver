from flask import Flask, render_template, request, abort
import os

app = Flask(__name__)

@app.route('/<name>')
def index(name):

    #get path of target file
    source_path = os.getcwd() + "/templates/" + name
    #check if file does not exist in templates
    if os.path.exists(source_path) == False:
        abort(404)
    return render_template(name), 200

@app.errorhandler(404)
def page_not_found(e):
    name = request.path
    #check if request contains forbidden characters
    Forbidden = False
    prev_char = ''
    for char in name:
        if char == '~':
            Forbidden = True
        elif char == '.' and prev_char == '.':
            Forbidden = True
        elif char == '/' and prev_char == '/':
            Forbidden = True
        prev_char = char
    if Forbidden:
        return render_template('403.html', title = "403"), 403
    return render_template('404.html', title = "404"), 404

@app.errorhandler(403)
def forbidden_page(e):
    return render_template('403.html', title = "403"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
