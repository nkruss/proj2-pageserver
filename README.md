# README #

Author - Noah Kruss

Contact Info - nkruss@uoregon.edu

Software Description - A simple web server to send the contents of target html file stored in "pages" folder with proper http response. 
	If requested URL contains Forbidden characters such as  (~, .., //) server will transmits 403 forbidden error). 
	If requested file is not in the templates folder error code 404 not found will be transmitted.
	Otherwise contents of target file will be transmitted
	
Note: if extra "/" are placed before link of target html file such as http://127.0.0.1:5000///test.html the extra "/" will be ignored.

